'''
Created on 28/03/2012

@author: asdrubal
'''
from PySide.QtGui import QTransform
from PySide.QtCore import QPoint

def rotar():
    ret_val = QTransform()
    ret_val.rotate(180)
    return ret_val
def escalar(ancho, alto, max_x, max_y):
    ret_val = QTransform()
    ret_val.scale(float(max_x / ancho), float(max_y / alto))
    return ret_val
def transformar(ancho, alto, max_x, max_y):
    matriz_rotar = rotar()
    matriz_escalar = escalar(ancho, alto, max_x, max_y)
    return  matriz_rotar * matriz_escalar
def punto_a_qpoint(punto):
    return QPoint(punto.x, punto.y)
