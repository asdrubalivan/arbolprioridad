#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 22/03/2012

@author: asdrubal
'''

from PySide.QtGui import QApplication, QMainWindow, QColor, QGraphicsScene, QPen, QBrush, QGraphicsView
from ui_pantalla import Ui_CVentanaPrincipal
from backend.arbol_prioridad import ArbolPrioridad
#import ArbolPrioridad
from backend.nodo import Nodo
#import Nodo
from backend.recorridos import Recorridos
#import Recorridos
import PySide.QtCore as QtCore
import random
import sys
from backend.rectangulo import Rectangulo
#import Rectangulo
from backend.errores import ErrorRectangulo
#import ErrorRectangulo

class ArbolGUI(QMainWindow):
    '''
    classdocs
    '''


    def __init__(self, parent=None):
        '''
        Constructor
        '''
        QMainWindow.__init__(self, parent)
        self.ui = Ui_CVentanaPrincipal()
        
        self.ui.setupUi(self)
        #Variables Auxiliares
        random.seed()
        self.val_max = 5000
        self.recorridos = Recorridos()
        self.arbol = ArbolPrioridad(self.val_max)
        #Conectar botones
        self.ui.boton_anadir_pto.clicked.connect(self.anadir_nodo)
        self.ui.boton_borrar_arbol.clicked.connect(self.borrar_arbol)
        self.ui.boton_puntos_alea.clicked.connect(self.generar_puntos)
        self.ui.boton_menor_x.clicked.connect(self.menor_punto_x)
        self.ui.boton_mayor_y.clicked.connect(self.mayor_punto_y)
        self.ui.boton_enumerar.clicked.connect(self.enumerar)
        self.ui.boton_interseccion.clicked.connect(self.interseccion)
        self.escena = QGraphicsScene()
        print self.ui.vista_puntos.width()
        #self.ui.vista_puntos.scale(0.6, 2)
        #self.ui.vista_puntos.rotate(270)
        self.escena.setSceneRect(0, 0, 5000, 5000)
        self.ui.vista_puntos.setScene(self.escena)
        self.ui.vista_puntos.setOptimizationFlag(QGraphicsView.DontSavePainterState, True)
        self.ui.vista_puntos.setViewportUpdateMode(QGraphicsView.ViewportUpdateMode.FullViewportUpdate)
        #Brushes y painter
        self.pincel = QPen(QColor(0, 205, 102))
        self.pincel_rect_may = QPen(QColor(0, 228, 255))
        self.pincel_rect_a = QPen(QColor(98, 107, 119))
        self.pincel_rect_b = QPen(QColor(255, 0, 223))
        self.pincel.setWidth(4)
        self.pincel.setCapStyle(QtCore.Qt.PenCapStyle.RoundCap)
        self.pincel_rect_may.setWidth(8)
        self.pincel_rect_may.setCapStyle(QtCore.Qt.PenCapStyle.RoundCap)
        self.pincel_rect_a.setWidth(6)
        self.pincel_rect_b.setWidth(6)
        self.pincel_rect_a.setCapStyle(QtCore.Qt.PenCapStyle.RoundCap)
        self.pincel_rect_b.setCapStyle(QtCore.Qt.PenCapStyle.RoundCap)
        self.tiza = QBrush(QColor(173, 216, 230))
        self.rect_punto_y = None
        self.rect_mayor_y = None
        self.rect_menor_x = None
        self.rect_punto_x = None
        self.rect_enum = None
        self.rect_1 = None
        self.rect_2 = None
        #Objeto Painter
    def anadir_nodo(self):
        self.arbol.insertar(Nodo(self.ui.coord_x.value(), self.ui.coord_y.value()))
        tupla = tuple(self.impr_inf(True))
        self.dibujar_puntos(tupla)
    def enumerar(self, retornar=False):
        if self.arbol.esta_vacio():
            return
        lista = []
        x0 = self.ui.x_pto1.value()
        x1 = self.ui.x_pto2.value()
        y0 = self.ui.y_pto1.value()
        y1 = self.ui.y_pto2.value()
        self.arbol.enumerar(x0, x1, y0, y1, lista)
        valido = True
        if len(lista) == 0:
            self.ui.resultado_puntos.setText(u"No hay puntos en ese rectángulo")
            valido = False
        self.borrar_rects()
        if valido:
            texto = ''.join([str(val) + '\n' for val in lista])
            self.ui.resultado_puntos.setText(texto)
            self.rect_enum = self.escena.addRect(x0, self.traducir_y_vista(y1), abs(x0 - x1), abs(y0 - y1), pen=self.pincel_rect_may)
        if not retornar:
            return None
        return lista
    def borrar_arbol(self):
        if self.arbol.esta_vacio():
            return
        self.arbol = ArbolPrioridad(self.val_max)
        self.recorridos = Recorridos()
        self.borrar_rects()
        self.escena.clear()
        print "Arbol borrado"
    def menor_punto_x(self):
        if self.arbol.esta_vacio():
            return
        x0 = self.ui.x_pto1.value()
        x1 = self.ui.x_pto2.value()
        y0 = self.ui.y_pto1.value()
        y1 = self.ui.y_pto2.value()
        valido = True
        val = self.arbol.min_x_rectangulo(x0, x1, y0, y1)
        if val.x is None and val.y is None:
            self.ui.resultado_puntos.setText(u"No hay puntos")
            valido = False
        self.ui.resultado_puntos.setText(str(val))
        self.borrar_rects()
        if valido:
            self.rect_punto_x = self.escena.addRect(val.x - 10, self.traducir_y_vista(val.y) - 10, 50, 50, pen=self.pincel)
            self.rect_punto_x.setToolTip(u"Punto con menor X en el rectángulo dado")
            self.rect_mayor_y = self.escena.addRect(x0, self.traducir_y_vista(y1), abs(x0 - x1), abs(y0 - y1), pen=self.pincel_rect_may)
        
    def mayor_punto_y(self):
        if self.arbol.esta_vacio():
            return
        x0 = self.ui.x_pto1.value()
        x1 = self.ui.x_pto2.value()
        y0 = self.ui.y_pto1.value()
        y1 = self.ui.y_pto2.value()
        valido = True
        val = self.arbol.max_y_rectangulo(x0, x1, y0, y1)
        if val.x is None and val.y is None:
            self.ui.resultado_puntos.setText("No hay puntos")
            valido = False
        self.ui.resultado_puntos.setText(str(val))
        self.borrar_rects()
        if valido:
            self.rect_punto_y = self.escena.addRect(val.x - 10, self.traducir_y_vista(val.y) - 10, 50, 50, pen=self.pincel)
            self.rect_punto_y.setToolTip(u"Punto con mayor Y en el rectángulo dado")
            self.rect_mayor_y = self.escena.addRect(x0, self.traducir_y_vista(y1), abs(x0 - x1), abs(y0 - y1), pen=self.pincel_rect_may)
    def interseccion(self):
        if self.arbol.esta_vacio():
            return
        rect1_x0 = self.ui.x_pto1_2.value()
        rect1_x1 = self.ui.x_pto2_2.value()
        rect1_y0 = self.ui.y_pto1_2.value()
        rect1_y1 = self.ui.y_pto2_2.value()
        rect2_x0 = self.ui.x_pto1_3.value()
        rect2_x1 = self.ui.x_pto2_3.value()
        rect2_y0 = self.ui.y_pto1_3.value()
        rect2_y1 = self.ui.y_pto2_3.value()
        lista = []
        try:
            intersec = self.arbol.enum_int(Rectangulo(rect1_x0, rect1_x1, rect1_y0, rect1_y1), \
                                            Rectangulo(rect2_x0, rect2_x1, rect2_y0, rect2_y1), \
                                            lista)
        except ErrorRectangulo:
            intersec = False
        self.borrar_rects()
        if not intersec:
            self.ui.textBrowser.setText(u"Intersección Nula")
        else:
            texto = ''.join([str(val) + '\n' for val in lista])
            self.ui.textBrowser.setText(texto)
            self.rect_1 = self.escena.addRect(rect1_x0, self.traducir_y_vista(rect1_y1), \
                                               abs(rect1_x0 - rect1_x1), abs(rect1_y0 - rect1_y1), \
                                               pen=self.pincel_rect_a)
            self.rect_2 = self.escena.addRect(rect2_x0, self.traducir_y_vista(rect2_y1), \
                                              abs(rect2_x0 - rect2_x1), abs(rect2_y0 - rect2_y1), \
                                               pen=self.pincel_rect_b)
    def impr_inf(self, retornar=False):
        print "Llamado infijo: "
        lista = []
        self.arbol.rec_inf(lista, False)
        for val in lista:
            print val
        if retornar:
            return lista
    def impr_pre(self, retornar=False):
        self.recorridos.prefijo = []
        lista = []
        self.arbol.rec_pre(lista, False)
        for val in lista:
            print val
        if retornar:
            return lista
    def generar_puntos(self):
        print "Generando puntos: "
        for x in xrange(0, self.ui.ptos_aleatorios.value()):
            self.arbol.insertar(Nodo(random.randint(0, self.val_max), random.randint(0, self.val_max)))
        lista = self.impr_inf(True)
        self.dibujar_puntos(tuple(lista))
    def dibujar_puntos(self, tupla):
        self.escena.clear()
        for val in tupla:
            elipse = self.escena.addEllipse(val.x, self.val_max - val.y, 30, 30, pen=self.pincel, brush=self.tiza)
            elipse.setToolTip(u"Círculo en la posición " + str(val))
        self.ui.vista_puntos.show()
    def borrar_rects(self):
        if self.arbol.esta_vacio():
            return
        self._borrar_rects([self.rect_enum, self.rect_mayor_y, \
                             self.rect_menor_x, self.rect_punto_x, \
                             self.rect_punto_y, self.rect_1, self.rect_2])
    def _borrar_rects(self, lista):
        for val in lista:
            if val is not None:
                try:
                    self.escena.removeItem(val)
                except:
                    pass
                val = None
    def traducir_y_vista(self, y):
        return self.val_max - y
def main():
    app = QApplication(sys.argv)
    ventana = ArbolGUI()
    ventana.show()
    app.exec_()
    sys.exit()

if __name__ == "__main__":
    print "Algo"
    main()
    
