'''
Created on 27/03/2012

@author: asdrubal
'''

from punto import Punto
from errores import ErrorRectangulo

class Rectangulo(object):
    '''
    classdocs
    '''


    def __init__(self, x0, x1, y0, y1):
        '''
        Constructor
        '''
        if x0 >= x1 or y0 >= y1:
            raise ErrorRectangulo
        self.punto1 = Punto(x0, y0)
        self.punto2 = Punto(x1, y1)
    def __str__(self):
        return "Punto 1: " + str(self.punto1) + " Punto 2: " + str(self.punto2)
    @staticmethod
    def interseccion(rect1, rect2):
        x0 = max(rect1.punto1.x, rect2.punto1.x)
        y0 = max(rect1.punto1.y, rect2.punto1.y)
        x1 = min(rect1.punto2.x, rect2.punto2.x)
        y1 = min(rect1.punto2.y, rect2.punto2.y)
        if x0 > x1 or y0 > y1:
            return None
        return Rectangulo(x0, x1, y0, y1)
