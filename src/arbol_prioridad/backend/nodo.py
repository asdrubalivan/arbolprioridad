#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 11/03/2012

@author: asdrubal
'''
import copy
from punto import Punto
class Nodo(Punto):
    '''
    classdocs
    '''


    def __init__(self, x, y):
        '''
        Constructor
        '''
        super(Nodo, self).__init__(x, y)
        self.izq = None
        self.der = None
    def __str__(self):
        return "x: " + str(self.x) + " y: " + str(self.y)
    @staticmethod
    def swap(punto1, punto2):
        temp = copy.copy(punto2)
        punto2.x = punto1.x
        punto2.y = punto1.y
        punto1.x = temp.x
        punto1.y = temp.y